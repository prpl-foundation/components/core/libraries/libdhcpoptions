/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <netinet/icmp6.h>

#include "util.h"
#include "raoption.h"

void raoption_parse(amxc_var_t* const parsed_info, uint8_t option, unsigned char* binval) {
    raoption_parse2(parsed_info, option, UINT32_MAX, binval);
}

void raoption_parse2(amxc_var_t* const parsed_info, uint8_t option, uint32_t binval_len, unsigned char* binval) {
    uint32_t i = 0;
    uint32_t len = 0;
    struct nd_opt_hdr* hdr = (struct nd_opt_hdr*) binval;

    when_null(parsed_info, exit);
    when_null(hdr, exit);
    when_true(hdr->nd_opt_len > binval_len, exit);

    len = hdr->nd_opt_len * 8;

    switch(option) {
    case   1:     // Source Link-layer Address (RFC 4861)
    case   2:     // Target Link-layer Address (RFC 4861)
        amxc_var_set(cstring_t, parsed_info, dhcpoption_parseLLAddress(&binval[2], len - 2));
        break;
    case   3:     // Prefix Information (RFC 4861)
    {
        struct nd_opt_prefix_info* pinfo = (struct nd_opt_prefix_info*) hdr;
        when_true(hdr->nd_opt_len != 4, exit);

        amxc_var_set_type(parsed_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_info, "PrefixLength", pinfo->nd_opt_pi_prefix_len);
        amxc_var_add_key(bool, parsed_info, "OnLink", pinfo->nd_opt_pi_flags_reserved & ND_OPT_PI_FLAG_ONLINK);
        amxc_var_add_key(bool, parsed_info, "Auto", pinfo->nd_opt_pi_flags_reserved & ND_OPT_PI_FLAG_AUTO);
        amxc_var_add_key(uint32_t, parsed_info, "ValidLifetime", ntohl(pinfo->nd_opt_pi_valid_time));
        amxc_var_add_key(uint32_t, parsed_info, "PreferredLifetime", ntohl(pinfo->nd_opt_pi_preferred_time));
        amxc_var_add_key(cstring_t, parsed_info, "Prefix", dhcpoption_parseIPv6Addr((unsigned char*) &pinfo->nd_opt_pi_prefix));
        break;
    }
    case   5:     // MTU (RFC 4861)
    {
        struct nd_opt_mtu* mtu = (struct nd_opt_mtu*) hdr;
        when_true(hdr->nd_opt_len != 1, exit);

        amxc_var_set(uint32_t, parsed_info, ntohl(mtu->nd_opt_mtu_mtu));
        break;
    }
    case  24:     // Route Information (RFC 4191)
    {
        uint8_t plen = binval[2];
        uint8_t prefix[16] = {0};

        if((hdr->nd_opt_len > 3) ||
           (( plen > 0) && ( hdr->nd_opt_len < 2)) ||
           (( plen > 64) && ( hdr->nd_opt_len < 3))) {
            goto exit;
        }

        amxc_var_set_type(parsed_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_info, "PrefixLength", plen);
        if((binval[3] & 0x08) != 0) {
            amxc_var_add_key(int8_t, parsed_info, "Preference", ((binval[3] & 0x10) != 0) ? -1 : 1);
        } else {
            amxc_var_add_key(int8_t, parsed_info, "Preference", 0);
        }
        amxc_var_add_key(uint32_t, parsed_info, "RouteLifetime", dhcpoption_parseUInt32(&binval[4]));
        if(plen > 0) {
            memcpy(prefix, &binval[8], (hdr->nd_opt_len - 1) * 8);
        }
        amxc_var_add_key(cstring_t, parsed_info, "Prefix", dhcpoption_parseIPv6Addr(prefix));
        break;
    }
    case  25:     // Recursive DNS Server (RFC 6106)
    {
        amxc_var_t* list = NULL;

        amxc_var_set_type(parsed_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, parsed_info, "Lifetime", dhcpoption_parseUInt32(&binval[4]));
        list = amxc_var_add_key(amxc_llist_t, parsed_info, "Servers", NULL);
        for(i = 8; i + 16 <= len; i += 16) {
            amxc_var_add(cstring_t, list, dhcpoption_parseIPv6Addr(&binval[i]));
        }
        break;
    }
    case  31:     // DNS Search List (RFC 6106)
    {
        amxc_var_t* list = NULL;
        uint32_t n = 0;

        amxc_var_set_type(parsed_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, parsed_info, "Lifetime", dhcpoption_parseUInt32(&binval[4]));
        list = amxc_var_add_key(amxc_llist_t, parsed_info, "Names", NULL);
        for(i = 8; i < len; i += n) {
            const char* name = dhcpoption_parseDomainName(&binval[i], len - i, &n);
            if(*name != '\0') {
                amxc_var_add(cstring_t, list, name);
            } else {
                break;
            }
        }
        break;
    }
    default:     // unknown option - simple ascii string if all bytes between 32 and 126, keep hexbinary prefixed with 0x otherwise
        amxc_var_set(cstring_t, parsed_info, dhcpoption_parseDefault(&binval[2], len - 2));
        break;
    }

exit:
    return;
}