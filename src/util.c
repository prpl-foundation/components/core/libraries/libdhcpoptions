/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <stdlib.h>

#include "util.h"
#include "str2bin.h"

char buf[3000];

char* util_buf(void) {
    return buf;
}

uint16_t dhcpoption_parseUInt16(unsigned char* binval) {
    uint16_t retval;
    memcpy(&retval, binval, 2);
    return ntohs(retval);
}

uint32_t dhcpoption_parseUInt32(unsigned char* binval) {
    uint32_t retval;
    memcpy(&retval, binval, 4);
    return ntohl(retval);
}

int32_t dhcpoption_parseInt32(unsigned char* binval) {
    int32_t retval;
    memcpy(&retval, binval, 4);
    return ntohl(retval);
}

const char* dhcpoption_parseIPv6Addr(unsigned char* binval) {
    struct in6_addr addr;
    memcpy(&addr, binval, 16);
    inet_ntop(AF_INET6, &addr, buf, sizeof(buf));
    return buf;
}

const char* dhcpoption_parseLLAddress(unsigned char* binval, uint32_t length) {
    uint32_t i;
    if(length > 256) {
        length = 256;
    }
    buf[0] = '\0';
    for(i = 0; i < length; i++) {
        sprintf(&buf[i * 3], "%02x:", binval[i]);
    }
    if(length != 0) {
        buf[length * 3 - 1] = '\0';
    }
    return buf;
}

const char* dhcpoption_parseAscii(unsigned char* binval, uint32_t length) {
    uint32_t i;
    if(length > (uint32_t) sizeof(buf) - 1) {
        length = sizeof(buf) - 1;
    }
    for(i = 0; i < length; i++) {
        if(!binval[i]) {
            break;
        } else if((binval[i] < 32) && (binval[i] > 126)) {
            buf[i] = '.';
        } else {
            buf[i] = binval[i];
        }
    }
    buf[i] = '\0';
    return buf;
}

const char* dhcpoption_parseHex(unsigned char* binval, uint32_t length) {
    uint32_t i;
    if(length > (uint32_t) (sizeof(buf) - 1) / 2) {
        length = (sizeof(buf) - 1) / 2;
    }
    for(i = 0; i < length; i++) {
        sprintf(&buf[i * 2], "%02x", binval[i]);
    }
    buf[i * 2] = '\0';
    return buf;
}

const char* dhcpoption_parseDefault(unsigned char* binval, uint32_t length) {
    uint32_t i;
    if(length > (uint32_t) sizeof(buf) - 1) {
        length = sizeof(buf) - 1;
    }

    for(i = 0; i < length; i++) {
        if(((binval[i] < 32) || (binval[i] > 126)) &&
           ((i != (length - 1)) || (binval[i] != 0))) {
            break;
        }
    }
    if(length == 0) {
        buf[0] = '\0';
    } else if(i < length) {
        dhcpoption_parseHex(binval, length);
    } else {
        dhcpoption_parseAscii(binval, length);
    }
    return buf;
}

const char* dhcpoption_parseIPv4Addr(unsigned char* binval) {
    sprintf(buf, "%u.%u.%u.%u", binval[0], binval[1], binval[2], binval[3]);
    return buf;
}

const char* dhcpoption_parseDomainName(unsigned char* binval, uint32_t length, uint32_t* parsed) {
    uint32_t m = 0;
    uint32_t n = 0;

    if(length > (uint32_t) sizeof(buf) - 1) {
        length = sizeof(buf) - 1;
    }

    for(n = 0; (n < length) && binval[n] && (n + binval[n] < length); n += m + 1) {
        m = binval[n];
        memcpy(&buf[n], &binval[n + 1], m);
        buf[n + m] = '.';
    }
    buf[n > 0 ? n - 1 : n] = '\0';

    if(parsed != NULL) {
        n++;
        *parsed = n;
    }
    return buf;
}

static uint32_t length_of_label(const char* domain_name) {
    uint32_t length = 0;
    char* end_of_label = strstr(domain_name, ".");

    if(end_of_label != NULL) {
        length = end_of_label - domain_name;
    } else {
        length = strlen(domain_name);
    }

    return length;
}

static uint32_t rfc1035_representation_of_domain_name(const char* domain_name, uint32_t offset) {
    char* copy = NULL;
    size_t total_length = 0;
    uint32_t written = 0;
    uint32_t pos = 0;
    uint32_t noffset = offset;

    when_str_empty(domain_name, exit);

    if(strlen(domain_name) > 255) {// rfc1035 limits the length of a domain name to 255 bytes
        goto exit;
    }
    copy = strdup(domain_name);
    total_length = strlen(copy);

    while(pos < total_length) {
        char buffer[4];
        uint32_t len = length_of_label(&copy[pos]);
        if(len == 0) {
            break;
        }
        if(len > 63) { // rfc1035 limits the length of a label to 63 bytes
            noffset = offset;
            goto exit;
        }

        snprintf(buffer, sizeof(buffer), "%u", len);
        str2bin_mult_uint8(buffer, &written, noffset);
        if(written == 0) {
            noffset = offset;
            goto exit;
        }
        noffset += written;

        copy[pos + len] = '\0';

        str2bin_mult_text(&copy[pos], &written, noffset);
        if(written == 0) {
            noffset = offset;
            goto exit;
        }
        noffset += written;

        pos += len + 1;
    }

    str2bin_mult_uint8("0", &written, noffset); // every domain name is terminated by a length byte of zero
    if(written == 0) {
        noffset = offset;
        goto exit;
    }
    noffset += written;

exit:
    free(copy);
    return noffset;
}

bool dhcpoption_convertDomainNameList(amxc_var_t* dest, amxc_var_t* src, uint32_t start) {
    amxc_var_t copy;
    amxc_string_t dest_str;
    uint32_t offset = start;
    bool rv = false;

    amxc_string_init(&dest_str, 0);
    amxc_var_init(&copy);

    when_null(src, exit);
    when_null(dest, exit);

    amxc_var_copy(&copy, src);

    when_failed(amxc_var_cast(&copy, AMXC_VAR_ID_LIST), exit);

    amxc_var_for_each(domain, &copy) {
        offset = rfc1035_representation_of_domain_name(GET_CHAR(domain, NULL), offset);
    }

    amxc_string_bytes_2_hex_binary(&dest_str, util_buf(), offset, NULL);
    amxc_var_push(amxc_string_t, dest, &dest_str);

    rv = true;

exit:
    amxc_var_clean(&copy);
    amxc_string_clean(&dest_str);
    return rv;
}
