/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <arpa/inet.h>

#include "str2bin.h"

#define STRING_EMPTY(X) ((X == NULL) || (*X == '\0'))

extern char buf[3000];

const unsigned char* str2bin_text(const char* val, uint32_t* length) {
    return str2bin_mult_text(val, length, 0);
}

const unsigned char* str2bin_mult_text(const char* val, uint32_t* length, uint32_t offset) {
    uint32_t len = strlen(val);

    if((offset < sizeof(buf)) && (len < (sizeof(buf) - offset))) {
        strcpy(&buf[offset], val);
        *length = len;
    } else {
        *length = 0;
    }

    return (const unsigned char*) buf;
}

const unsigned char* str2bin_buffer(const char* val, uint32_t* length) {
    if((*length > 0) && (*length < sizeof(buf))) {
        memcpy(buf, val, *length);
    } else {
        *length = 0;
    }
    return (const unsigned char*) buf;
}

static bool str2bin_check_number(const char* val, bool nospaces, bool positive) {
    bool error = false;
    while(*val != '\0') {
        if((nospaces == true) && (*val == ' ')) {
            error = true;
            break;
        }
        if((positive == true) && (*val == '-')) {
            error = true;
            break;
        }
        if(isalpha(*val)) {
            error = true;
            break;
        }
        val++;
    }
    return error;
}

const unsigned char* str2bin_uint8(const char* val, uint32_t* length) {
    return str2bin_mult_uint8(val, length, 0);
}

const unsigned char* str2bin_mult_uint8(const char* val, uint32_t* length, uint32_t offset) {
    uint32_t tmp = 0;
    uint8_t numeric = 0;

    when_false_status((sizeof(buf) - offset) >= 1, exit, *length = 0);

    if(str2bin_check_number(val, true, true) == true) {
        *length = 0;
    } else if(sscanf(val, "%u", &tmp) == 1) {
        if(tmp < 256) {
            numeric = (uint8_t) tmp;
            memcpy(&buf[offset], &numeric, 1);
            *length = 1;
        } else {
            *length = 0;
        }
    } else {
        *length = 0;
    }

exit:
    return (const unsigned char*) buf;
}

// convert integer reprensented in string to binary, in network byte order (big endianess)
const unsigned char* str2bin_uint32(const char* val, uint32_t* length) {
    uint32_t numeric = 0;
    if(str2bin_check_number(val, true, true) == true) {
        *length = 0;
    } else if(sscanf(val, "%u", &numeric) == 1) {
        numeric = htonl(numeric);
        memcpy(buf, &numeric, 4);
        *length = 4;
    } else {
        *length = 0;
    }
    return (const unsigned char*) buf;
}

// convert integer reprensented in string to binary, in network byte order (big endianess)
const unsigned char* str2bin_int32(const char* val, uint32_t* length) {
    uint32_t retval = 0;
    int32_t numeric = 0;
    if(str2bin_check_number(val, true, false) == true) {
        *length = 0;
    } else if(sscanf(val, "%d", &numeric) == 1) {
        retval = htonl(*(uint32_t*) &numeric);
        memcpy(buf, &retval, 4);
        *length = 4;
    } else {
        *length = 0;
    }
    return (const unsigned char*) buf;
}

// convert 0/1 binary 0x01/0x01
const unsigned char* str2bin_bool(const char* val, uint32_t* length) {
    uint8_t* retval = (uint8_t*) buf;
    if(((val[0] == '0') || (val[0] == '1')) && (val[1] == '\0')) {
        *retval = (uint8_t) (val[0] - '0');
        *length = 1;
    } else {
        *length = 0;
    }
    return (const unsigned char*) buf;
}

const unsigned char* str2bin_ipv4_addr(const char* val, uint32_t* length, uint32_t offset) {
    unsigned char* ip = NULL;
    *length = 0;
    when_false(offset < (sizeof(buf) - 4), exit);

    ip = (unsigned char*) &buf[offset];
    if(sscanf(val, "%hhu.%hhu.%hhu.%hhu", &ip[0], &ip[1], &ip[2], &ip[3]) != 4) {
        memset(ip, 0, 4);
    } else {
        *length = 4;
    }
exit:
    return (const unsigned char*) buf;
}

const unsigned char* str2bin_single_ipv4_addr(const char* str_value, uint32_t* length) {
    return str2bin_ipv4_addr(str_value, length, 0);
}

const unsigned char* str2bin_mult_ipv4_addr(const char* str_value, uint32_t* length) {
    const unsigned char* retval = NULL;
    amxc_string_t str_val;
    amxc_var_t ip_list;
    uint32_t nr_of_ip_addr = 0;

    amxc_var_init(&ip_list);
    amxc_string_init(&str_val, 0);
    // comma or space seperated IPv4 strings
    amxc_string_set(&str_val, str_value);
    amxc_string_replace(&str_val, " ", ",", UINT32_MAX);
    amxc_string_csv_to_var(&str_val, &ip_list, NULL);

    amxc_var_for_each(var, &ip_list) {
        const char* ipaddr = amxc_var_constcast(cstring_t, var);
        uint32_t len = 0;
        if(STRING_EMPTY(ipaddr)) {
            continue;
        }
        retval = str2bin_ipv4_addr(ipaddr, &len, nr_of_ip_addr * 4);
        if(len == 0) {
            break;
        }
        nr_of_ip_addr++;
    }
    *length = 4 * nr_of_ip_addr;
    amxc_var_clean(&ip_list);
    amxc_string_clean(&str_val);
    return retval;
}

const unsigned char* str2bin_mult_pairs_ipv4_addr(const char* str_value, uint32_t* length) {
    const unsigned char* retval = str2bin_mult_ipv4_addr(str_value, length);

    // length should be a multple of 8 bytes (2 IPv4 addresses)
    *length &= 0xFFFFFFF8;
    return retval;
}

// convert integer reprensented in string to binary, in network byte order (big endianess)
const unsigned char* str2bin_uint16(const char* val, uint32_t* length, uint32_t offset) {
    uint16_t numeric = 0;
    if(str2bin_check_number(val, true, true) == true) {
        *length = 0;
    } else if(sscanf(val, "%hu", &numeric) == 1) {
        numeric = htons(numeric);
        memcpy(&buf[offset], &numeric, 2);
        *length = 2;
    } else {
        *length = 0;
    }
    return (const unsigned char*) buf;
}

// convert integer reprensented in string to binary, in network byte order (big endianess)
const unsigned char* str2bin_single_uint16(const char* val, uint32_t* length) {
    return str2bin_uint16(val, length, 0);
}

const unsigned char* str2bin_mult_uint16(const char* str_value, uint32_t* length) {
    const unsigned char* retval = NULL;
    amxc_string_t str_val;
    amxc_var_t num_list;
    uint32_t nr_of_uint16 = 0;

    amxc_var_init(&num_list);
    amxc_string_init(&str_val, 0);
    // comma or space seperated IPv4 strings
    amxc_string_set(&str_val, str_value);
    amxc_string_replace(&str_val, " ", ",", UINT32_MAX);
    amxc_string_csv_to_var(&str_val, &num_list, NULL);

    amxc_var_for_each(var, &num_list) {
        const char* numeric = amxc_var_constcast(cstring_t, var);
        uint32_t len = 0;
        if(STRING_EMPTY(numeric)) {
            continue;
        }
        retval = str2bin_uint16(numeric, &len, nr_of_uint16 * 2);
        if(len == 0) {
            break;
        }
        nr_of_uint16++;
    }
    *length = 2 * nr_of_uint16;
    amxc_var_clean(&num_list);
    amxc_string_clean(&str_val);
    return retval;
}
