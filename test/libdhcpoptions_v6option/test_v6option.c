/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc_macros.h>

#include "test_v6option.h"
#include "v4v6option.h"

static void test_ipv6_option(uint8_t option, const char* data_to_send, amxc_var_t* const ret_object);

int test_setup(UNUSED void** state) {

    return 0;
}

int test_teardown(UNUSED void** state) {

    return 0;
}

static void test_ipv6_option(uint8_t option, const char* data_to_send, amxc_var_t* const ret_object) {
    uint32_t length = 0;
    unsigned char* data_to_send_bin = dhcpoption_option_convert2bin(data_to_send, &length);
    dhcpoption_v6parse(ret_object, option, length, data_to_send_bin);
    free(data_to_send_bin);
}

void test_ipv6_option9(UNUSED void** state) {

    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv6_option(9, "18ff", parsed_lease_info);// 18ff = string

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_string_equal(amxc_var_constcast(cstring_t, parsed_lease_info), "18ff");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv6_option25(UNUSED void** state) {

    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv6_option(25, "AAAAAAAABBBBBBBBCCCCCCCCAAAA000400000001", parsed_lease_info);// AAAAAAAA = IAID, BBBBBBBB = T1, CCCCCCCC = T2, AAAA = option type, 0004 option length, 00000001 = optionvalue

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_int_equal(GET_UINT32(parsed_lease_info, "IAID"), 0xAAAAAAAA);
    assert_int_equal(GET_UINT32(parsed_lease_info, "T1"), 0xBBBBBBBB);
    assert_int_equal(GET_UINT32(parsed_lease_info, "T2"), 0xCCCCCCCC);

    amxc_var_t* option = amxc_var_get_first(GET_ARG(parsed_lease_info, "Options"));
    assert_int_equal(amxc_var_constcast(uint16_t, GET_ARG(option, "Type")), 0xAAAA);
    assert_string_equal(GET_CHAR(option, "Value"), "00000001");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv6_option64_aftr(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t dest;
    amxc_var_t src;
    uint32_t len = 0;
    unsigned char* binary = NULL;

    amxc_var_init(&var);
    amxc_var_init(&dest);
    amxc_var_init(&src);

    amxc_var_set(cstring_t, &src, "aftr.example.com");
    dhcpoption_v6_option_to_hex(64, &dest, &src);
    assert_string_equal(GET_CHAR(&dest, NULL), "0461667472076578616D706C6503636F6D00");

    binary = dhcpoption_option_convert2bin(GET_CHAR(&dest, NULL), &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 64, len, binary);
    assert_string_equal(GET_CHAR(&var, NULL), "aftr.example.com");

    amxc_var_clean(&var);
    amxc_var_clean(&dest);
    amxc_var_clean(&src);
    free(binary);
}

void test_ipv6_option1_clientid(UNUSED void** state) {
    amxc_var_t var;
    uint32_t len = 0;
    const char* hex_str = "0001000a00030001021018010f01";
    unsigned char* binary = NULL;

    amxc_var_init(&var);

    binary = dhcpoption_option_convert2bin(hex_str, &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 1, len, binary);

    assert_int_equal(GET_UINT32(&var, "HardwareType"), 10);
    assert_string_equal(GET_CHAR(&var, "LLAddress"), "02:10:18:01:0f:01");
    assert_int_equal(GET_UINT32(&var, "Time"), 196609);
    assert_int_equal(GET_UINT32(&var, "Type"), 1);

    amxc_var_clean(&var);
    free(binary);
}

void test_ipv6_option2_serverid(UNUSED void** state) {
    amxc_var_t var;
    uint32_t len = 0;
    const char* hex_str = "0002000e000100012ac6cd9b000ec6e1e989";
    unsigned char* binary = NULL;

    amxc_var_init(&var);

    binary = dhcpoption_option_convert2bin(hex_str, &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 2, len, binary);

    assert_int_equal(GET_UINT32(&var, "Enterprise"), 917505);
    assert_string_equal(GET_CHAR(&var, "Identifier"), "00012ac6cd9b000ec6e1e989");
    assert_int_equal(GET_UINT32(&var, "Type"), 2);

    amxc_var_clean(&var);
    free(binary);
}

void test_ipv6_option6_oro(UNUSED void** state) {
    amxc_var_t var;
    const amxc_llist_t* list = NULL;
    uint32_t len = 0;
    const char* hex_str = "00170040";
    unsigned char* binary = NULL;

    amxc_var_init(&var);

    binary = dhcpoption_option_convert2bin(hex_str, &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 6, len, binary);

    list = amxc_var_constcast(amxc_llist_t, &var);
    assert_int_equal(amxc_llist_size(list), 2);
    assert_int_equal(GETP_UINT32(&var, "0"), 23);
    assert_int_equal(GETP_UINT32(&var, "1"), 64);

    amxc_var_clean(&var);
    free(binary);
}

void test_ipv6_option7_preference(UNUSED void** state) {
    amxc_var_t var;
    uint32_t len = 0;
    const char* hex_str = "ff";
    unsigned char* binary = NULL;

    amxc_var_init(&var);

    binary = dhcpoption_option_convert2bin(hex_str, &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 7, len, binary);

    assert_int_equal(GET_UINT32(&var, NULL), 255);

    amxc_var_clean(&var);
    free(binary);
}

void test_ipv6_option20_reconfigure_accept(UNUSED void** state) {
    amxc_var_t var;
    uint32_t len = 0;
    const char* hex_str = "2ac6cb9b";
    unsigned char* binary = NULL;

    amxc_var_init(&var);

    binary = dhcpoption_option_convert2bin(hex_str, &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 20, len, binary);

    assert_int_equal(GET_BOOL(&var, NULL), 1);

    amxc_var_clean(&var);
    free(binary);
}

void test_ipv6_option23_recursive_name_server(UNUSED void** state) {
    amxc_var_t var;
    const amxc_llist_t* list = NULL;
    uint32_t len = 0;
    const char* hex_str = "fec00000000000010000000000000001";
    unsigned char* binary = NULL;

    amxc_var_init(&var);

    binary = dhcpoption_option_convert2bin(hex_str, &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 23, len, binary);

    list = amxc_var_constcast(amxc_llist_t, &var);
    assert_int_equal(amxc_llist_size(list), 1);
    assert_string_equal(GETP_CHAR(&var, "0"), "fec0:0:0:1::1");

    amxc_var_clean(&var);
    free(binary);
}

void test_ipv6_option24(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t dest;
    amxc_var_t src;
    const amxc_llist_t* list = NULL;
    uint32_t len = 0;
    unsigned char* binary = NULL;

    amxc_var_init(&var);
    amxc_var_init(&dest);
    amxc_var_init(&src);

    amxc_var_set(cstring_t, &src, "be.softathome.com,softathome.com,rd.softathome.com");
    dhcpoption_v6_option_to_hex(64, &dest, &src);
    assert_string_equal(GET_CHAR(&dest, NULL), "0262650A736F66746174686F6D6503636F6D000A736F66746174686F6D6503636F6D000272640A736F66746174686F6D6503636F6D00");

    binary = dhcpoption_option_convert2bin(GET_CHAR(&dest, NULL), &len);
    assert_non_null(binary);
    dhcpoption_v6parse(&var, 24, len, binary);

    list = amxc_var_constcast(amxc_llist_t, &var);
    assert_int_equal(amxc_llist_size(list), 3);
    assert_string_equal(GETI_CHAR(&var, 0), "be.softathome.com");
    assert_string_equal(GETI_CHAR(&var, 1), "softathome.com");
    assert_string_equal(GETI_CHAR(&var, 2), "rd.softathome.com");

    amxc_var_clean(&var);
    amxc_var_clean(&dest);
    amxc_var_clean(&src);
    free(binary);
}

void test_ipv6_option39(UNUSED void** state) {
    amxc_var_t dest;
    amxc_var_t src;

    amxc_var_init(&dest);
    amxc_var_init(&src);

    amxc_var_set_type(&src, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &src, "Flag", "0");
    amxc_var_add_key(cstring_t, &src, "Domains", "boris-ThinkPad-13-2nd-Gen");

    dhcpoption_v6_option_to_hex(39, &dest, &src);
    assert_string_equal(GET_CHAR(&dest, NULL), "0019626F7269732D5468696E6B5061642D31332D326E642D47656E00"); // 00 (1 byte flag) 19 (length = 25) 62...6E (hostname) 00 (terminated by 0x00 byte)

    amxc_var_set_type(&src, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &src, "Flag", "0");
    amxc_var_add_key(cstring_t, &src, "Domains", "boris-ThinkPad-13-2nd-Gen.com");

    dhcpoption_v6_option_to_hex(39, &dest, &src);
    assert_string_equal(GET_CHAR(&dest, NULL), "0019626F7269732D5468696E6B5061642D31332D326E642D47656E03636F6D00"); // ... 03 (length) 63...6D (label com) 00

    amxc_var_set_type(&src, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &src, "Flag", "0");
    amxc_var_add_key(cstring_t, &src, "Domains", "boris-ThinkPad-13-2nd-Gen,boris-ThinkPad-13-2nd-Gen.com");

    dhcpoption_v6_option_to_hex(39, &dest, &src);
    assert_string_equal(GET_CHAR(&dest, NULL), "0019626F7269732D5468696E6B5061642D31332D326E642D47656E0019626F7269732D5468696E6B5061642D31332D326E642D47656E03636F6D00"); // ... 62...6E (domain1) 00 (every domain is terminated with 0x00 byte) 19 (length label1 of domain2) 62...6D (domain2) 00

    amxc_var_clean(&dest);
    amxc_var_clean(&src);
}