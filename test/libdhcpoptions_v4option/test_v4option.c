/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc_macros.h>

#include "test_v4option.h"
#include "v4v6option.h"

/**********************************************************
* Function Prototypes
**********************************************************/

static void test_ipv4_option(uint8_t option, const char* data_to_send, amxc_var_t* const ret_object);

/**********************************************************
* Functions
**********************************************************/

int test_setup(UNUSED void** state) {

    return 0;
}

int test_teardown(UNUSED void** state) {

    return 0;
}

static void test_ipv4_option(uint8_t option, const char* data_to_send, amxc_var_t* const ret_object) {
    uint32_t length = 0;
    unsigned char* data_to_send_bin = dhcpoption_option_convert2bin(data_to_send, &length);
    dhcpoption_v4parse(ret_object, option, length, data_to_send_bin);
    free(data_to_send_bin);
}

void test_ipv4_option54(UNUSED void** state) {

    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(54, "c0a80001", parsed_lease_info); // c0a80001 = "192.168.0.1"

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_string_equal(amxc_var_constcast(cstring_t, parsed_lease_info), "192.168.0.1");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option2(UNUSED void** state) {

    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(2, "2EE0EEEE", parsed_lease_info); //2EE0EEEE = 32bit value

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_int_equal(amxc_var_constcast(int32_t, parsed_lease_info), 0x2EE0EEEE);

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option76(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(76, "c0a80001ffffff00", parsed_lease_info); // c0a80001 = "192.168.0.1", ffffff00 = "255.255.255.0"

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* second = amxc_var_get_index(parsed_lease_info, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(amxc_var_constcast(cstring_t, first), "192.168.0.1");
    assert_string_equal(amxc_var_constcast(cstring_t, second), "255.255.255.0");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option21(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(21, "c0a80001ffffff0001010101ffff0000", parsed_lease_info);// c0a80001 = "192.168.0.1", ffffff00 = "255.255.255.0", 01010101 = "1.1.1.1", ffff0000 = "255.255.0.0"

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* second = amxc_var_get_index(parsed_lease_info, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(GET_CHAR(first, "Address"), "192.168.0.1");
    assert_string_equal(GET_CHAR(first, "Mask"), "255.255.255.0");
    assert_string_equal(GET_CHAR(second, "Address"), "1.1.1.1");
    assert_string_equal(GET_CHAR(second, "Mask"), "255.255.0.0");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option33(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(33, "c0a80001ffffff0001010101ffff0000", parsed_lease_info);// c0a80001 = "192.168.0.1", ffffff00 = "255.255.255.0", 01010101 = "1.1.1.1", ffff0000 = "255.255.0.0"

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* second = amxc_var_get_index(parsed_lease_info, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(GET_CHAR(first, "Destination"), "192.168.0.1");
    assert_string_equal(GET_CHAR(first, "Router"), "255.255.255.0");
    assert_string_equal(GET_CHAR(second, "Destination"), "1.1.1.1");
    assert_string_equal(GET_CHAR(second, "Router"), "255.255.0.0");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option55(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(55, "0103060f1f212b2c2e2f7779f9fc", parsed_lease_info);// 01 = 1, 03 = 3, 06 = 6, 0f = 15, 1f = 31, ...

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    uint8_t numbers_to_check[] = {1, 3, 6, 15, 31, 33, 43, 44, 46, 47, 119, 121, 249, 252};
    uint8_t times_checked = 0;
    uint8_t i = 0;

    amxc_var_cast(parsed_lease_info, AMXC_VAR_ID_LIST);
    amxc_var_for_each(var, parsed_lease_info) {
        uint8_t var_value = amxc_var_constcast(uint8_t, var);
        for(i = 0; i < (sizeof(numbers_to_check) / sizeof(uint8_t)); i++) {
            if(var_value == numbers_to_check[i]) {
                times_checked++;
                break;
            }
        }
    }
    assert_int_equal((sizeof(numbers_to_check) / sizeof(uint8_t)), times_checked);
    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option125(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(125, "a000621f0a01080a000a0a0a000a0b", parsed_lease_info); // a000621F = enterprise-number1, 0a = length, 01 = option, 08 = lengthoption, 0a000a0a0a000a0b = data

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first_var = amxc_var_get_first(parsed_lease_info);
    amxc_var_t* data_var = amxc_var_get_first(GET_ARG(first_var, "Data"));

    assert_int_equal(GET_UINT32(first_var, "Enterprise"), 0xa000621F);
    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(data_var, "Code")), 0x01);
    assert_string_equal(GET_CHAR(data_var, "Data"), "0a000a0a0a000a0b");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option60(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(60, "7465737420737472696e67", parsed_lease_info);// 7465737420737472696e67 = "test string"

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_string_equal(amxc_var_constcast(cstring_t, parsed_lease_info), "test string");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option77(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(77, "0401010101020303", parsed_lease_info); // 04 = length first part, 01010101 = first part, 02 length second part, 0303 = second part

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* second = amxc_var_get_index(parsed_lease_info, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(amxc_var_constcast(cstring_t, first), "01010101");
    assert_string_equal(amxc_var_constcast(cstring_t, second), "0303");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option57(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(57, "0f0f", parsed_lease_info); // 0f0f = 16bit value

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_int_equal(amxc_var_constcast(uint16_t, parsed_lease_info), 0x0f0f);

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option39(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(39, "01", parsed_lease_info); // 01 = boolean (true)

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_true(amxc_var_constcast(bool, parsed_lease_info));

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option53(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(53, "01", parsed_lease_info); // 01 = 1bit type value

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_int_equal(amxc_var_constcast(uint8_t, parsed_lease_info), 0x01);

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option59(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(59, "0f0f0f0f", parsed_lease_info);// 0f0f0f0f = 32bit time integer

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_int_equal(amxc_var_constcast(uint32_t, parsed_lease_info), 0x0f0f0f0f);

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option25(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(25, "0f0e0f0f", parsed_lease_info);// 0f0e = first 16bit value, 0f0f = second 16bit value

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* second = amxc_var_get_index(parsed_lease_info, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_int_equal(amxc_var_constcast(uint16_t, first), 0x0f0e);
    assert_int_equal(amxc_var_constcast(uint16_t, second), 0x0f0f);

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option56(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(56, "74657374", parsed_lease_info);// 74657374 = test

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_string_equal(amxc_var_constcast(cstring_t, parsed_lease_info), "test");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option43(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(43, "F1030a7e7eF2020101FF", parsed_lease_info);// F1 = type, 03 = data length, 0a7e7e = data, F2 = type, 02 = data length, 0101 = data, FF = end

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* second = amxc_var_get_index(parsed_lease_info, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(first, "Code")), 0xF1);
    assert_string_equal(GET_CHAR(first, "Data"), "0a7e7e");

    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(second, "Code")), 0xF2);
    assert_string_equal(GET_CHAR(second, "Data"), "0101");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option61(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(61, "30223344556677", parsed_lease_info);// 30 = type, 223344556677 = ClientIdentifier

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "Type")), 0x30);
    assert_string_equal(GET_CHAR(parsed_lease_info, "ClientIdentifier"), "22:33:44:55:66:77");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option90(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(90, "AABBCCDDEEFFAABBCCDDEEFF", parsed_lease_info);// AA = Protocol, BB = Algorithm, CC = RDM, DDEEFFAABBCCDDEE = ReplayDetection, FF =  AuthenticationInformation

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "Protocol")), 0xAA);
    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "Algorithm")), 0xBB);
    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "RDM")), 0xCC);
    assert_string_equal(GET_CHAR(parsed_lease_info, "ReplayDetection"), "ddeeffaabbccddee");
    assert_string_equal(GET_CHAR(parsed_lease_info, "AuthenticationInformation"), "ff");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option119(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(119, "0262650A736F66746174686F6D6503636F6D000A736F66746174686F6D6503636F6D000272640A736F66746174686F6D6503636F6D00", parsed_lease_info);

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    assert_string_equal(GETI_CHAR(parsed_lease_info, 0), "be.softathome.com");
    assert_string_equal(GETI_CHAR(parsed_lease_info, 1), "softathome.com");
    assert_string_equal(GETI_CHAR(parsed_lease_info, 2), "rd.softathome.com");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option120(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(120, "00047465737406737472696e6700", parsed_lease_info);//00 = encoding value(0 or 1), 04 = length(test), 74657374 = "test", 06 = length(string), 737472696e67 = "string", 00 = end

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, first), "test.string");

    //TEST 2
    test_ipv4_option(120, "01c0a80001ffffff00", parsed_lease_info);//01 = encoding value(0 or 1), c0a80001 = "192.168.0.1", ffffff00 = "255.255.255.0"
    assert_false(amxc_var_is_null(parsed_lease_info));

    first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* second = amxc_var_get_index(parsed_lease_info, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(amxc_var_constcast(cstring_t, first), "192.168.0.1");
    assert_string_equal(amxc_var_constcast(cstring_t, second), "255.255.255.0");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option121(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(121, "18c0a800ffffff01", parsed_lease_info);// 18 = subnet 24(/24), c0a800 = "192.168.0" (last .0 found by subnet value), ffffff01 = "255.255.255.01" (router address)

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);

    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(first, "PrefixLength")), 24);
    assert_string_equal(GET_CHAR(first, "Destination"), "192.168.0.0");
    assert_string_equal(GET_CHAR(first, "Router"), "255.255.255.1");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option124(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(124, "a000621f080a0014050a001406", parsed_lease_info); // a000621F = enterprise-number1, 08 = lengthoption, 0a000a0a0a000a0 = data

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    amxc_var_t* first = amxc_var_get_index(parsed_lease_info, 0, AMXC_VAR_FLAG_DEFAULT);

    assert_int_equal(GET_UINT32(first, "Enterprise"), 0xa000621F);
    assert_string_equal(GET_CHAR(first, "Data"), "0a0014050a001406");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option212(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(212, "18ffa801010101010101010101010101010a01020304", parsed_lease_info);// 18 = subnet 24(/24), ff = 6rd prefix length, a801010101010101010101010101010a = 6rdPrefix, 01020304 = 6rdBRIPv4Address

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));

    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "IPv4MaskLen")), 0x18);
    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "6rdPrefixLen")), 0xff);
    assert_string_equal(GET_CHAR(parsed_lease_info, "6rdPrefix"), "a801:101:101:101:101:101:101:10a");

    amxc_var_t* first = amxc_var_get_index(GET_ARG(parsed_lease_info, "6rdBRIPv4Address"), 0, AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, first), "1.2.3.4");

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option252(UNUSED void** state) {
    //INIT
    amxc_var_t* parsed_lease_info = NULL;
    amxc_var_new(&parsed_lease_info);
    test_ipv4_option(252, "18ff", parsed_lease_info);// 18 = SubOption0, ff = Networktype

    //TEST
    assert_false(amxc_var_is_null(parsed_lease_info));
    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "SubOption0")), 0x18);
    assert_int_equal(amxc_var_constcast(uint8_t, GET_ARG(parsed_lease_info, "Networktype")), 0xff);

    amxc_var_delete(&parsed_lease_info);
}

void test_ipv4_option12(UNUSED void** state) {
    amxc_var_t host_name;
    amxc_var_init(&host_name);

    test_ipv4_option(12, "626f7269732d5468696e6b5061642d31332d326e642d47656e", &host_name);
    assert_string_equal(GET_CHAR(&host_name, NULL), "boris-ThinkPad-13-2nd-Gen");

    test_ipv4_option(12, "626F7269732D5468696E6B5061642D31332D326E642D47656E", &host_name);
    assert_string_equal(GET_CHAR(&host_name, NULL), "boris-ThinkPad-13-2nd-Gen");

    amxc_var_clean(&host_name);
}