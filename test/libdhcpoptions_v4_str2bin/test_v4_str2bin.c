/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc_macros.h>

#include "test_v4_str2bin.h"
#include "v4v6option.h"

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

int test_setup(UNUSED void** state) {

    return 0;
}

int test_teardown(UNUSED void** state) {

    return 0;
}

void test_v4_str2bin_option2(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[4] = { 0xFF, 0xFF, 0xFF, 0x9C };
    const unsigned char test2[4] = { 0x00, 0x0F, 0x42, 0x40 };

    ret = dhcpoption_v4_str2bin(2, "-100", &len);
    assert_int_equal(len, 4);
    assert_memory_equal(test1, ret, 4);

    ret = dhcpoption_v4_str2bin(2, "1000000", &len);
    assert_int_equal(len, 4);
    assert_memory_equal(test2, ret, 4);
}

void test_v4_str2bin_option3(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[4] = { 0xC0, 0xA8, 0x00, 0x01 };
    const unsigned char test2[8] = { 0xC0, 0xA8, 0x01, 0x01, 0x0A, 0x00, 0x65, 0x01 };

    ret = dhcpoption_v4_str2bin(3, "192.168.0.1", &len);
    assert_int_equal(len, 4);
    assert_memory_equal(test1, ret, 4);

    ret = dhcpoption_v4_str2bin(3, "192.168.1.1 10.0.101.1", &len);
    assert_int_equal(len, 8);
    assert_memory_equal(test2, ret, 8);

    ret = dhcpoption_v4_str2bin(3, "192.168.1.1,10.0.101.1", &len);
    assert_int_equal(len, 8);
    assert_memory_equal(test2, ret, 8);

    ret = dhcpoption_v4_str2bin(3, "192.168.1.1, 10.0.101.1", &len);
    assert_int_equal(len, 8);
    assert_memory_equal(test2, ret, 8);
}

void test_v4_str2bin_option12(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[7] = { 0x4F, 0x70, 0x65, 0x6E, 0x57, 0x72, 0x74 };

    ret = dhcpoption_v4_str2bin(12, "OpenWrt", &len);
    assert_int_equal(len, 7);
    assert_memory_equal(test1, ret, 7);
}

void test_v4_str2bin_option13(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[2] = { 0xC3, 0x50 };

    ret = dhcpoption_v4_str2bin(13, "50000", &len);
    assert_int_equal(len, 2);
    assert_memory_equal(test1, ret, 2);

    ret = dhcpoption_v4_str2bin(13, "-50000", &len);
    assert_int_equal(len, 0);
}

void test_v4_str2bin_option19(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[1] = { 0x00 };
    const unsigned char test2[1] = { 0x01 };

    ret = dhcpoption_v4_str2bin(19, "a", &len);
    assert_int_equal(len, 0);

    ret = dhcpoption_v4_str2bin(19, "3", &len);
    assert_int_equal(len, 0);
    ret = dhcpoption_v4_str2bin(19, "", &len);
    assert_int_equal(len, 0);

    ret = dhcpoption_v4_str2bin(19, "0", &len);
    assert_int_equal(len, 1);
    assert_memory_equal(test1, ret, 1);

    ret = dhcpoption_v4_str2bin(19, "1", &len);
    assert_int_equal(len, 1);
    assert_memory_equal(test2, ret, 1);
}


void test_v4_str2bin_option21(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[8] = { 0xC0, 0xA8, 0x01, 0x0A, 0xFF, 0xFF, 0xFF, 0x00 };


    ret = dhcpoption_v4_str2bin(21, "192.168.1.10 255.255.255.0", &len);
    assert_int_equal(len, 8);
    assert_memory_equal(test1, ret, 8);

    ret = dhcpoption_v4_str2bin(21, "192.168.1.10 255.255.255.0 192.168.1.20", &len);
    // additional address is discarded because of missing mask.
    assert_int_equal(len, 8);
    assert_memory_equal(test1, ret, 8);

    ret = dhcpoption_v4_str2bin(3, "192.168.1.10, 255.255.255.0", &len);
    assert_int_equal(len, 8);
    assert_memory_equal(test1, ret, 8);
}

void test_v4_str2bin_option25(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[6] = { 0x01, 0xF4, 0x03, 0xE8, 0x07, 0xD0 };

    ret = dhcpoption_v4_str2bin(25, "500,1000,2000", &len);
    assert_int_equal(len, 6);
    assert_memory_equal(test1, ret, 6);
}

void test_v4_str2bin_option37(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[6] = { 0x1E };

    ret = dhcpoption_v4_str2bin(37, "30", &len);
    assert_int_equal(len, 1);
    assert_memory_equal(test1, ret, 1);

    ret = dhcpoption_v4_str2bin(37, "-30", &len);
    assert_int_equal(len, 0);

    ret = dhcpoption_v4_str2bin(37, "300", &len);
    assert_int_equal(len, 0);
}

void test_v4_str2bin_option38(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char test1[4] = { 0x00, 0x00, 0xEA, 0x60 };

    ret = dhcpoption_v4_str2bin(38, "60000", &len);
    assert_int_equal(len, 4);
    assert_memory_equal(test1, ret, 4);

    ret = dhcpoption_v4_str2bin(38, " 60000", &len);
    assert_int_equal(len, 0);

    ret = dhcpoption_v4_str2bin(38, "60000a", &len);
    assert_int_equal(len, 0);
}

void test_v4_str2bin_option43(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const char test1[4] = { 0x00, 0x00, 0xEA, 0x60 };

    len = 4;
    ret = dhcpoption_v4_str2bin(43, test1, &len);
    assert_int_equal(len, 4);
    assert_memory_equal(test1, ret, 4);

}

void test_v4_str2bin_option50(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;
    const unsigned char exp[4] = { 0xC0, 0xA8, 0x00, 0x0A };

    ret = dhcpoption_v4_str2bin(50, "192.168.0.10", &len);
    assert_int_equal(len, 4);
    assert_memory_equal(exp, ret, 4);
}

void test_v4_str2bin_option119(UNUSED void** state) {
    uint32_t len = 0;
    const unsigned char* ret = NULL;

    ret = dhcpoption_v4_str2bin(119, "be.softathome.com,softathome.com,rd.softathome.com", &len);
    assert_string_equal(ret, "0262650A736F66746174686F6D6503636F6D000A736F66746174686F6D6503636F6D000272640A736F66746174686F6D6503636F6D00");
}
