Clang's libfuzzer
=================
The libfuzzer will generate random data for the option parsers. These kind of tests are powerful but they do not test for correctness. The cmocka tests are also important.

Just run this command to run the fuzzers and generate the coverage html report:
```
make -C test/fuzzer run
```

compile fuzzer_foo with code coverage
-------------------------------------
```
clang -g -O1 -fsanitize=fuzzer -fprofile-instr-generate -fcoverage-mapping fuzzer_foo.c ../../src/*.c -I../../include -I../../include_priv -lamxc -o fuzzer_foo
```

run fuzzer_foo for max 60 seconds
---------------------------------
```
LLVM_PROFILE_FILE="fuzzer_foo.profraw" ./fuzzer_foo -max_total_time=60
```

get code coverage
-----------------
```
llvm-profdata-11 merge -sparse fuzzer_foo.profraw -o fuzzer_foo.profdata
llvm-cov-11 report ./fuzzer_foo -instr-profile=fuzzer_foo.profdata
llvm-cov-11 show ./fuzzer_foo -instr-profile=fuzzer_foo.profdata -format=html > report.html
```

how to debug crash
------------------
```
~/workspace/lib_dhcpoptions/test/fuzzer$ hd ./crash-943b15d51d66a0ef719f3bd5b7643210000a4f50 
00000000  19 ff ff ff ff ff ff ff  ff ff ff ff ff ff ff ff  |................|
00000010  ff ff ff ff ff ff ff ff  ff ff ff ff ff ff ff 0f  |................|
00000020
```
use this input in cmocka unit test