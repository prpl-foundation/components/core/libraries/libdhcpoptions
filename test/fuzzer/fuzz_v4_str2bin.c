#include <amxc/amxc.h>
#include "v4v6option.h"

int LLVMFuzzerTestOneInput(const uint8_t* data, size_t len) {
    uint32_t length = 0;
    dhcpoption_v4_str2bin(data[0], (const char*) &data[len > 1 ? 1 : 0], &length);
    return 0;  // Values other than 0 and -1 are reserved for future use.
}